﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.IO;

namespace ServerTest
{
    class TimeProcessor
    {
        //words to regular
        string[] nowWords = { "сейчас","now"};
        string[] secondsWords = { "секунду","секунды","секунд"};
        string[] minutesWords = { "минуту","минуты","минут"};
        string[] hoursWords = { "час","часа","часов"};
        string[] daysWords = { "день","дня","дней"};
        string[] weeksWords = { "неделю","недели","недель"};
        string[] monthsWords = { "месяц","месяца","месяцев"};
        string[] yearsWords = { "год","года","лет"};
        public string Processing(string _s)
        {
            DateTime date = new DateTime();
            date = DateTime.Now;
            foreach (var x in nowWords)
            {
                if (Regex.IsMatch(_s, x))
                {
                    return date.ToString("yyyy.MM.dd HH:mm:ss").Substring(2); //now return
                }
            }
            foreach (var x in secondsWords)
            {
                Regex regex = new Regex("[0-9]* " + x); // regex with regex words
                if(Regex.IsMatch(_s, "[0-9]* "+x)) // regex match?
                {
                    Regex numRegex = new Regex("[0-9]*"); //num from regex
                    if (Int32.TryParse(numRegex.Match(regex.Match(_s).ToString()).ToString(), out int Add))
                        date = date.AddSeconds(Add); //try parsing this num (Add)
                    else
                        date = date.AddSeconds(1); //parsing = false
                    break;
                }
            }
            foreach (var x in minutesWords)
            {
                Regex regex = new Regex("[0-9]* " + x);
                if (Regex.IsMatch(_s, "[0-9]* " + x))
                {
                    Regex numRegex = new Regex("[0-9]*");
                    if (Int32.TryParse(numRegex.Match(regex.Match(_s).ToString()).ToString(), out int Add))
                        date = date.AddMinutes(Add);
                    else
                        date = date.AddMinutes(1);
                    break;
                }
            }
            foreach (var x in hoursWords)
            {
                Regex regex = new Regex("[0-9]* " + x);
                if (Regex.IsMatch(_s, "[0-9]* " + x))
                {
                    Regex numRegex = new Regex("[0-9]*");
                    if (Int32.TryParse(numRegex.Match(regex.Match(_s).ToString()).ToString(), out int Add))
                        date = date.AddHours(Add);
                    else
                        date = date.AddHours(1);
                    break;
                }
            }
            foreach (var x in daysWords)
            {
                Regex regex = new Regex("[0-9]* " + x);
                if (Regex.IsMatch(_s, "[0-9]* " + x))
                {
                    Regex numRegex = new Regex("[0-9]*");
                    if (Int32.TryParse(numRegex.Match(regex.Match(_s).ToString()).ToString(), out int Add))
                        date = date.AddDays(Add);
                    else
                        date = date.AddDays(1);
                    break;
                }
            }
            foreach (var x in weeksWords)
            {
                Regex regex = new Regex("[0-9]* " + x);
                if (Regex.IsMatch(_s, "[0-9]* " + x))
                {
                    Regex numRegex = new Regex("[0-9]*");
                    if (Int32.TryParse(numRegex.Match(regex.Match(_s).ToString()).ToString(), out int Add))
                        date = date.AddDays(Add * 7);
                    else
                        date = date.AddDays(7);
                    break;
                }
            }
            foreach (var x in monthsWords)
            {
                Regex regex = new Regex("[0-9]* " + x);
                if (Regex.IsMatch(_s, "[0-9]* " + x))
                {
                    Regex numRegex = new Regex("[0-9]*");
                    if (Int32.TryParse(numRegex.Match(regex.Match(_s).ToString()).ToString(), out int Add))
                        date = date.AddMonths(Add);
                    else
                        date = date.AddMonths(1);
                    break;
                }
            }
            foreach (var x in yearsWords)
            {
                Regex regex = new Regex("[0-9]* " + x);
                if (Regex.IsMatch(_s, "[0-9]* " + x))
                {
                    Regex numRegex = new Regex("[0-9]*");
                    if (Int32.TryParse(numRegex.Match(regex.Match(_s).ToString()).ToString(), out int Add))
                        date = date.AddYears(Add);
                    else
                        date = date.AddYears(1);
                    break;
                }
            }
            return date.ToString("yyyy.MM.dd HH:mm:ss").Substring(2);
        }
    }
    class ServerManagment
    {
        static int port = 7777;//standart port
        static string ip = "127.0.0.1";
        void Logs(string _s, string _ip)
        {
            if (_s == "xswzaq")
                _s = "[Client-Code] Stop using";
            else if (_s == "qazwsx")
                _s = "[Client-Code] Start using";
            StreamWriter LogsWriter = File.AppendText("ServerLogs.txt");
            LogsWriter.WriteLine("{0} | {1}: {2}",DateTime.Now.ToString("dd.MM.yyyy HH:mm"),_ip,_s);
            Console.WriteLine("{0} | {1}: {2}",DateTime.Now.ToString("dd.MM.yyyy HH:mm"),_ip,_s);
            LogsWriter.Close(); LogsWriter.Dispose();
        }
        public string AnswerChoosing(string _message)
        {
            if ("qazwsx" == _message)
                return "OK";
            else if ("xswqaz" == _message)
                return "OK";
            
            else
            {
                TimeProcessor timeProcessor = new TimeProcessor();
                return timeProcessor.Processing(_message);
            }
        }
       public void StartListening(int _port)
        {
            if (_port > 0 && _port < 65535)
                port = _port;
            else
                Console.WriteLine("Произошла ошибка, выбран стандартный порт 7777");
            Console.WriteLine("Сервер запущен. Ожидание запросов. Включена запись в файл ServerLogs.txt");
            IPEndPoint IpPoint = new IPEndPoint(IPAddress.Parse(ip), port); //create server-ip point
            Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // socket for listening information
            listenSocket.Bind(IpPoint); //connect listenSocket to server-ip point
            listenSocket.Listen(10);
            while (true)
            {
                Socket messageHandler = listenSocket.Accept();

                try
                {
                StringBuilder builder = new StringBuilder(); //string Data Builder
                int bytes = 0;
                byte[] data = new byte[256]; //data container
                do
                {
                        bytes = messageHandler.Receive(data); //Data from socket to buffer
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                }
                while (messageHandler.Available > 0);
                Logs(builder.ToString(), messageHandler.RemoteEndPoint.ToString());
                //answer from server
                string Message = AnswerChoosing(builder.ToString());
                data = Encoding.Unicode.GetBytes(Message);
                messageHandler.Send(data);
                messageHandler.Shutdown(SocketShutdown.Both);
                messageHandler.Close();
                }
                catch (SocketException)
                {
                    Logs("[Server-Message] Принудительный разрыв соединения", messageHandler.RemoteEndPoint.ToString());
                }
            }

}
    }
    class Program
    {
        static void Main(string[] args)
        {
            ServerManagment server = new ServerManagment();
            Console.WriteLine("Введите номер порта:");
            int.TryParse(Console.ReadLine(), out int port);
            server.StartListening(port);
        }
    }
}
